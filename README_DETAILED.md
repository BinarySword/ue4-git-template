#### NOTE: If you're wanting to look at this file properly outside of [Bitbucket's rendered Markup](https://bitbucket.org/BinarySword/[INSERT]/overview), you can do so by opening the .md file with Notepad & than coping the text into this [Markup Preview Site](http://dillinger.io/)

Welcome to [INSERT]!
========================

**CURRENT BUILD:** v0.0.1_pre-alpha ([Unreal Engine v4.25.1](https://www.unrealengine.com/425-release-notes-overview))

**CURRENT BRANCHES:**

- [develop](https://bitbucket.org/BinarySword/[INSERT]/commits/branch/develop) - The integration branch used for development. Feature branches are merged back into this branch.

- [master](https://bitbucket.org/BinarySword/[INSERT]/commits/branch/master) - The branch used for deploying releases. Typically, this branches from the development branch and changes are merged back into the development branch.

**NOTE:** When creating a new branch follow these [guidelines](https://confluence.atlassian.com/display/STASH0210/Using+branches+in+Stash) to see whether the work you have in mind for the branch falls under feature/, release/ bugfix/ or hotfix/

---------------------------------------------------------------------------------------------------------------------------------

Project Overview/Description
----------------------------

**[INSERT]** is a [INSERT GENRE(S)] game being developed on Unreal Engine 4. The game consists of....


Project URL links
-----------------

**Production:**

- **[Confluence Cloud](https://binarysword.atlassian.net/wiki/display/[INSERT])**

- **[JIRA Cloud](https://binarysword.atlassian.net/projects/[INSERT])**

- **[HacknPlan](https://app.hacknplan.com)**

- **[Bitbucket Cloud](https://bitbucket.org/BinarySword/[INSERT])**

**AND/OR**

- **[GitHub](https://github.com/BinarySword/[INSERT])**


**Social:**

- **[Website](http://www.[INSERT].com)**

- **[Twitter](http://www.twitter.com/[INSERT])**

- **[Facebook](http://www.facebook.com/[INSERT])**

- **[YouTube](https://www.youtube.com/channel/[INSERT])**

- **[Vimeo](http://vimeo.com/[INSERT])**

- **[Tumblr](http://[INSERT].tumblr.com)**

- **[IndieDB](http://www.indiedb.com/games/[INSERT])**

- **[Steam Page](http://steamcommunity.com/groups/[INSERT])**

- **[Steam Greenlight](http://steamcommunity.com/sharedfiles/filedetails/?id=[INSERT])**



Supporting Markup (.MD) documentation
-------------------------------------

- **Best Work Practices:**
  - [Online Viewing](https://bitbucket.org/BinarySword/[INSERT]/src)
  - [Offline Viewing](\BEST_WORK_PRACTICES.md)



How to get started
------------------

The current **[INSERT]** project is based on **Unreal Engine version [INSERT]**  You can always get this version of the engine using the [Unreal Engine Launcher](https://www.unrealengine.com/dashboard), or you can grab the [full engine source](https://github.com/EpicGames/UnrealEngine/releases) from GitHub and compile it yourself.

**Downloading the [INSERT] project:**

1. Download the project by clicking the **Download** button on this page.
 - **NOTE:** Click the **Clone** button on this page if you wish to actively contribute to the project (see **"Contributing through Atlassian Stash"** below for more info)
2. Unzip the files to a folder on your computer.
 - **NOTE:** It is **recommended** that you save the project in a location with a **short path name** e.g. **C:\[INSERT]** over say **C:\Repositories\Project[INSERT]**.
3. Load up [Unreal Editor](https://www.unrealengine.com/dashboard).
4. On the Project Browser screen, click **Browse** and navigate to the folder where unzipped the files.
5. Choose the **[INSERT].uproject** file.  

You'll now be looking at the very latest work-in-progress version of the game!



Setting up the project
----------------------

**Windows:**

 1. Be sure to have [Visual Studio 2015](https://www.visualstudio.com/vs-2015-product-editions) installed.  You can use any desktop version of [Visual Studio 2015](https://www.visualstudio.com/), including the free version called Community 2015.
 - **NOTE:** You need [DirectX End-User Runtimes (June 2010)](http://www.microsoft.com/en-us/download/details.aspx?id=8109) to avoid compilation errors.  Most users will already have this, if you have ever installed DirectX runtimes to play a game.
 2. Right click on **[INSERT].uproject** to generate C++ project files.
 3. You can now open up **[INSERT].uproject** in the editor!
- **NOTE:** If **[INSERT].uproject** still fails to launch in the editor, open the **[INSERT].sln** Microsoft Visual Studio Solution, **press F7** to build the solution & then **repeat step 3** after the build has completed.

**Mac OSX:**

1. Be sure to have [Xcode 5.1](https://itunes.apple.com/us/app/xcode/id497799835) installed.
2. Right click on **[INSERT].uproject** > services and click on **Generate xcode project** to generate C++ project files.
3. Open **[INSERT].codeproj** and select **[INSERT]Editor - Mac** from the scheme selector.
4. You can now open up **[INSERT].uproject** in the editor!

**Linux:**

- You can follow this tutorial: https://wiki.unrealengine.com/Building_On_Linux



Setting up Git Version Control in editor
----------------------------------------

1. Open **Window->Plugins** from the main menu of the **Unreal Editor**.
2. Navigate to the **Built-in/Editor/Source Control** or the **Installed/Editor/Source Control** sub-category, you should see the **Git** plugin in the list.
3. Enable the plugin and restart the editor if requested to do so.
4. Click on the **circular red icon** in the top right corner of the Unreal Editor.
5. Select **Git** from the drop-down.
6. If you installed [Git](http://git-scm.com/downloads) the **Git Executable** should've been auto-detected, otherwise you need to specify the location of the Git executable (Git.exe on Windows, may be just Git elsewhere).
7. Press the **Accept Settings** button to enable the Mercurial source control provider.
8. Congratulations! You’re ready to start using **Git Version Control in editor** :)

You now have direct access for submitting & receiving work directly in editor. If you need further information on what each **Asset Status Icon** means, than to read this [step-by-step guide](https://docs.unrealengine.com/latest/INT/Engine/UI/SourceControl/index.html#statusicons).


More info on how to use Git in UE4 along with recommended Git GUI clients can be found here: https://wiki.unrealengine.com/Git_source_control_(Tutorial) (Written by yours truely: BenjaminDSmithy)



Installed UE4 project plugins
-----------------------------

- NONE

**NOTE:** All plugins are located within the repository at the following path: **\[INSERT]\Plugins**



Developer tools
---------------

- NONE

**NOTE:** Standalone tools are located within the repository at the following path: **\[INSERT]\Tools**



Contributing through Bitbucket Cloud
------------------------------------

The Main way to contribute changes to the repository is to **Commit** [staged or individually selected] & **Push** the changes back to the server.

While the other option is to send a Stash [Pull Request](https://confluence.atlassian.com/display/STASH/Using+pull+requests+in+Stash) for more code specific additions to the repository.

**IMPORTANT NOTE:** Please remember to include the **JIRA Ticket [e.g. [INSERT]-1]** assigned to your specific task [if any] in the **Commit Message**, along with an overview of the changes you've made in that commit. 

**To get started using Stash:**

1. [Install a Git client](http://www.sourcetreeapp.com/) on your computer.
2. Next, you'll want to create your own **clone** of **[INSERT]** by clicking the __Clone button__ in the top right of this page.
3. Use the SourceTree program to **Pull** the project's files to a folder on your machine.
4. You can now open up **[INSERT].uproject** in the editor!
5. Using the [SourceTree](http://www.sourcetreeapp.com/) program, you can easily **submit contributions** back up to the remote **Branch**.  These files will be visible to all subscribers, and you can advertise your changes on [JIRA](https://binarysword.atlassian.net/issues/) & showcase your work on [Confluence](https://binarysword.atlassian.net/wiki/).
 - **NOTE:** You can download the [Git Client]() if you wish to have even easier access to some of the essential JIRA features on your computer & with the added bonus of being able to work in **offline mode**.
6. When you're ready to send the changes to the **[INSERT]** lead team members for review, simply create a [Pull Request](https://confluence.atlassian.com/display/STASH/Using+pull+requests+in+Stash).



JIRA Clients
------------

**Windows / Mac:**

In order to start using JIRA Client, the user needs to:

1. Download and install the latest JIRA Client (site edition) from [JIRA Client Download Page](http://almworks.com/get-jiraclient-site) on ALM Works website.
2. Start JIRA Client & click **New JIRA Connection**.
3. Enter our JIRA's address (https://bitbucket.org/BinarySword) and your login/password.
4. Select the projects you work with, in this case **[INSERT]** (it is advised not to select more than 20 projects).
5. Name your connection **"JIRA - Binary Sword Pty Ltd"**
6. Rick-click to delete **"User Queries"** and **"Sample Queries"**
7. Select **"JIRA - Binary Sword Pty Ltd"** and ether right-click or press Ctrl+D to **Create Distribution**.
8. Repeat **7.** on **JIRA - Binary Sword Pty Ltd-->Project-->[INSERT]** and Create Distribution for **Assignee**, **Status** and **Priority**.
9. **Congratulations!** you're ready to start using **JIRA Client** in your day-to-day work pipeline on the project :)

**NOTE:** You can gain access to an easy to use time tracking feature by pressing **Ctrl+M**.



**iOS:**

- [JIRA Cloud By Atlassian](https://itunes.apple.com/au/app/jira-cloud/id1006972087)

- [JIRA Connect Professional By MobilityStream, LLC](https://itunes.apple.com/au/app/jira-connect-professional/id590540499?mt=8)


**Android:**

- [JIRA Cloud By Atlassian](https://play.google.com/store/apps/details?id=com.atlassian.android.jira.core&hl=en)

- [JIRA Connect Professional By MobilityStream, LLC](https://play.google.com/store/apps/details?id=com.mobilitystream.android.jiraconnectpro&hl=en)


**Copyright © 2012-2020 by Binary Sword Pty Ltd. All rights reserved.**