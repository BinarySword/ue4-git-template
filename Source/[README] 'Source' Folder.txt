`/Source`
---------

C++ source code is stored under the `/Source` path. As with most other directories, this directory is managed by standard git 
(and not `git-lfs`). That means no blobs. Do not put here any `.dll`s, `.exe`s, `.zip`s and other binaries. Only text files are allowed.
Generated text files can reside in the local `/Source` dir, but should be ignored by git with additional entries in `.gitignore`.


Find out more at: '/BEST_WORK_PRACTICES.md'
