`/Content`
----------

Game assets in Unreal Engine formats, `.uasset` and `.umap`. Only those two file types are allowed, everything else is ignored.


Find out more at: '/BEST_WORK_PRACTICES.md'
