﻿#### NOTE: If you're wanting to look at this file properly outside of [Bitbucket's rendered Markup](https://bitbucket.org/BinarySword/[INSERT]/overview), you can do so by opening the .md file with Notepad & than coping the text into this [Markup Preview Site](http://dillinger.io/).

Repo Structure
--------------

Repository structure is fixed, and it only has a few toplevel directories. Every other directory or file is ignored.

- `/Source`
- `/Config`
- `/Plugins`
- `/Content`
- `/RawContent`

`git-lfs` management rules are mostly defined for file types, and not *paths*, however there can entire paths marked to be managed by `git-lfs`. Without a special note, expect only type-based rules apply to a directory.



`/Source`
---------

C++ source code is stored under the `/Source` path. As with most other directories, this directory is managed by standard git (and not `git-lfs`). That means no blobs. Do not put here any `.dll`s, `.exe`s, `.zip`s and other binaries. Only text files are allowed.
Generated text files can reside in the local `/Source` dir, but should be ignored by git with additional entries in `.gitignore`.



`/Config`
---------

Engine and game config files.



`/Plugins`
----------

Game plugins. Every plugin lives in a subdirectory of the `/Plugins` dir. A plugin internal directory structure is not strictly documented, so there are no assumptions on how a plugin is structured.
It may be useful to use git submodules to manage plugins in a more robust manner.

It is expected that each plugin will have it's own `.gitignore` file in it's subdirectory, as well other required specific git tweaks.



`/Content`
----------

Game assets in Unreal Engine formats, `.uasset` and `.umap`. Only those two file types are allowed, everything else is ignored.



`/RawContent`
-------------

**This directory is managed entirely by `git-lfs`.**

`/RawContent` is a directory where you store assets in their source formats, in contrast to `/Content`, where assets are stored in the engine format (after the import). Having an asset in a source format is useful when you're still making updates to it.



How to use
----------

1. Set up `git` and `git-lfs`.
2. Copy `.gitignore` and `.gitattributes` to your project.
3. Change the `Game.uproject` in `.gitignore` to your `.uproject` file name.



Caveats
-------

Take special care when working with plugins. Plugin structure is not very well defined, so you will be able to mess the git repo up with big files if you commit them to a plugin directory.



JIRA Ticket best practices
--------------------------

- Always include a JIRA Ticket ID (for this project its **INSERT-**) if there is not a task assigned to your work, than please create one on [JIRA](https://binarysword.atlassian.net/browse/INSERT)
 - **NOTE:** There are some exceptions (needs to be worked on)


 
Project file naming conventions
-------------------------------

**NOTE: Some elements of this document were taken and/or modified from the [naming convention page](https://wiki.unrealengine.com/Assets_Naming_Convention) over at Unreal’s wiki.**

General Naming

- All names in **English**.
- All asset dependencies should be in the **same folder**. (Except for shared assets)
- Asset type determines prefix.
 - Blueprint is **BP_AssetName01**
- Certain types (e.g. textures) use a suffix to specify sub-types. 
 - **T_Grass01_N** for normal maps
- Use underscores to split type from identifier and numeric values.
 - **SM_DoorHandle01**
- Use numeric values with 2 digits.
 - **SM_Pipe01**



Prefixes
---------	

| Asset Type                    | Prefix | Example                    | Comment                                                                                                                        |
| ------------------------------|--------|----------------------------|--------------------------------------------------------------------------------------------------------------------------------|
| Blutilities                   | BT_    | BT_EditorFunction01        | A blutility is almost exactly the same as a normal blueprint except instead of running at run-time; you can run in the editor. |
| Blueprint                     | BP_    | BP_WallLight01             |                                                                                                                                |
| Blueprint Interface           | BPI_   | BPI_InventoryItem01        |                                                                                                                                |
| Characters                    | CH_    | CH_NPC_GenricCharacter01   |                                                                                                                                |
| Material                      | M_     | M_Grass01                  |                                                                                                                                |
| Material Instance             | MI_    | MI_Grass01                 |                                                                                                                                |
| Material Function             | MF_    | MF_CheapContrast           | Not numbered.                                                                                                                  |
| Material Parameter Collection | MPC_   | MPC_EnvironmentSettings_01 |                                                                                                                                |
| Static Mesh                   | SM_    | SM_Wall01                  |                                                                                                                                |
| Skeletal Mesh                 | SK_    | SK_Character01             |                                                                                                                                |
| Texture                       | T_     | T_Grass01_D                | Has suffix for texture types. See suffixes table.                                                                              |
| Particle System               | PS_    | PS_Fire01                  |                                                                                                                                |
| Physics Material              | PM_    | PM_Dirt01                  | Not numbered.                                                                                                                  |
| Sound                         | S_     | S_HitImpact01              |                                                                                                                                |
| Sound Cue                     | SC_    | SC_HitImpact01             |                                                                                                                                |
| Spreadsheets                  | SS_    | SS_ExampleTemplate         | Not numbered. Similar to convention in code (enum EWeaponType).                                                                |
| CurveTables                   | CT_    | CT_LevelUp_Progression     | Not numbered. Similar to convention in code (enum EWeaponType).                                                                |
| DataTables                    | DT_    | DT_DamageMultiplier        |                                                                                                                                |
| Enumeration                   | E      | EWeaponType                | Not numbered. Similar to convention in code (enum EWeaponType).                                                                |
| Render Target                 | RT_    | RT_CameraCapturePoint01    |                                                                                                                                |
| Apex Destructible Asset       | AD_    | AD_BreakableDoor01         | Has suffix for texture types. See suffixes table.                                                                              |
| Apex Cloth Asset              | AC_    | AC_TearableCurtin01        | Has suffix for texture types. See suffixes table.                                                                              |
| SpeedTree                     | ST_    | ST_OakTree01               | Has suffix for texture types. See suffixes table.                                                                              |
| Lens Flare                    | LF_    | LF_JJAbram_Favorite01      | J.J. Abrams would be proud :)                                                                                                  |
| Vehicles                      | VH_    | VH_MosterTruck01           |                                                                                                                                |


Suffixes
--------

### Textures

Texture types all use the T_ prefix.

| Texture type          | Suffix |
|-----------------------|--------|
| Albedo Map            | _ALB   |
| Colour Map            | _C     |
| Defuse Map            | _D     |
| Normal Map            | _N     |
| Emissive Map          | _E     |
| Mask Map              | _M     |
| Roughness Map         | _R     |
| Metallic Map          | _MT    |
| Specular Map          | _S     |
| Gloss Map             | _G     |
| Displacement Map      | _DP    |
| Ambient Occlusion Map | _AO    |
| Height Map            | _H     |
| Flow Map              | _F     |
| Light Map (custom)    | _L     |



### Animations

These types have no prefix.

| Asset type          | Suffix                                   |
|---------------------|------------------------------------------|
| Animation Blueprint | _AnimBlueprint                           |
| Physics Asset       | _Physics                                 |
| Skeleton            | _Skeleton                                |
| FaceFx              | _FaceFX                                  |
| Blendspace          | use descriptive name: _AimOffsets        |
| AnimMontage         | use descriptive name: _Death _Equip etc. |



### Texture Masks

RGB Mask for environment:

- R = Metallic
- G = Roughness
- B = Ambient Occlusion

RGB Mask for characters: 
- R = Metallic
- G = Roughness
- B = Subsurface Opacity

RGB Mask for character's Hair:
- R = Hair Alpha
- G = Specular/Roughness map
- B = Anisotropic direction map



## Content Directories

| Folder Path                                     | Description                                                                                         |
|-------------------------------------------------|-----------------------------------------------------------------------------------------------------|
| Content\Base                                    | Base assets (e.g. master materials) material functions and other "foundations" assets.              |
| Content\Characters                              | Character meshes / blueprints and skeletons.                                                        |
| ...................... Characters\NPC           | NPCs (Non-playable characters).                                                                     |
| ...................... Characters\Player        | Player character(s).                                                                                |
| ...................... Characters\FaceFX        | FaceFX assests.                                                                                     |
| Content\Dev                                     | Development assets / mockup meshes / special textures / markers and icons. Not part of final build. |
| Content\Effects                                 | Particle effects and dependencies.                                                                  |
| Content\Environment                             | Environment assets (meshes – materials – textures).                                                 |
| ...................... Environment\Background   | Backgrounds.                                                                                        |
| ...................... Environment\Buildings    | Buildings (simple or procedural).                                                                   |
| ...................... Environment\Foliage      | Foliage.                                                                                            |
| ...................... Environment\Props        | Various props.                                                                                      |
| ...................... Environment\Sky          | Skies.                                                                                              |
| ...................... Environment\Landscape    | Terrain assets.                                                                                     |
| ...................... Environment\Water        | Water meshes and materials.                                                                         |
| Content\Experimental                            | --**EXPERIMENTAL**-- Very early stage code, blueprints, assets, materials, etc...                   |
| Content\Gameplay                                | Gameplay specific assets (e.g. flag mesh & dependencies for Capture The Flag).                      |
| Content\LensFlares                              | J.J. Abrams would be proud.                                                                         |
| Content\Maps                                    | Parent maps folder.                                                                                 |
| ...................... Maps\MP                  | Multiplayer cooperative maps/levels.                                                                |
| ...................... Maps\SP\Episode(_Number) | Singleplayer game episodes, where (_Number) is 01, 02, 03, etc.                                     |
| ...................... Maps\Showcase            | Showcase maps e.g. asset zoo's, game mechanic previews, etc. For both internal & public             |
| ...................... Maps\TestMaps            | Test maps, map prototypes and other levels not for production.                                      |
| Content\Morphs                                  | MorphTargets.                                                                                       |
| Content\PostProcess                             | Post process chains and its dependencies.                                                           |
| Content\Sound                                   | Sounds and sound cues.                                                                              |
| Content\Spreadsheets                            | Excel (.xlsm) & comma separated variable (.csv) files.                                              |
| Content\UI                                      | Menu and HUD assets.                                                                                |
| Content\Weapons                                 | Weapons and projectiles.                                                                            |

**NOTE:** If moving & renaming **binary files**, please move all the files, commit, then rename the files & commit again. **This will prevent Git** from thinking they are completely new files, revisioning them as new & **making the repository bigger**.



Coding Standards
----------------

Epic has set up a [Coding Standards](https://docs.unrealengine.com/latest/INT/Programming/Development/CodingStandard/index.html) page at the Unreal documentation pages.


### Class Names

- #TODO



### Class Directories

- #TODO



**Copyright © 2012-2020 by Binary Sword Pty Ltd. All rights reserved.**