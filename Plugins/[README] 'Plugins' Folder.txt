`/Plugins`
----------

Game plugins. Every plugin lives in a subdirectory of the `/Plugins` dir. A plugin internal directory structure is not strictly documented, so there are no assumptions on how a plugin is structured.
It may be useful to use git submodules to manage plugins in a more robust manner.

It is expected that each plugin will have it's own `.gitignore` file in it's subdirectory, as well other required specific git tweaks.


Find out more at: '/BEST_WORK_PRACTICES.md'
