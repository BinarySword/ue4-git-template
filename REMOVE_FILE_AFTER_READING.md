﻿TODO LIST
---------

- Replace all [INSERT], **[INSERT]** and INSERT mentions from README_DETAILED.md with project name.
- Replace Game.uproject in '/.gitignore' file otherwise the file will not be found in your git client of choice.


**NOTE:** to properly edit .md files you can do so by opening the .md file with Notepad & than coping the text into this [Markup Preview Site](http://github-markup.dfilimonov.com)